(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.stickybits = {
    attach: function (context) {
      stickybits('.sticky');
    }
  };
})(jQuery, Drupal);
