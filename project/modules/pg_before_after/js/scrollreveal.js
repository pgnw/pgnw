(function(Drupal) {
    'use strict';
    Drupal.behaviors.pg_before_after_scrollreveal = {
        attach: function() {
            // JavaScript
            window.sr = ScrollReveal();
            sr.reveal('.before-after-card', { reset: true } );
        }
    }
})(Drupal);
