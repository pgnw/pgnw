<?php

namespace Drupal\pg_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class PgFooterBlock
 * @Block(
 *   id = "pg_footer",
 *   admin_label = @Translation("PG footer"),
 * )
 */
class PgFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'pg_footer',
    ];
  }

}
