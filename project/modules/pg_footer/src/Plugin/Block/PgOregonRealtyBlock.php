<?php

namespace Drupal\pg_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class PgOregonRealtyBlock
 * @Block(
 *   id = "pg_oregon_realty",
 *   admin_label = @Translation("PG Oregon Realty Block"),
 * )
 */
class PgOregonRealtyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'pg_oregon_realty',
    ];
  }

}
