<?php
/**
 * Drush Aliases
 *
 * This file provides for the automatic generation of site aliases based
 * on the file layout and configuration of the Docker hosting environment.
 *
 * Site alias for tuned Drush usage with the 'pgnw' site.
 */

$host = getenv('APP_DOMAIN');

$aliases['pgnw'] = array(
  'uri' => $host ? $host : 'http://www.pgnw.vm/',
  'root' => '/var/www/build/html',
  'path-aliases' => array(
    '%drush' => '/var/www/vendor/drush/drush',
    '%drush-script' => '/var/www/vendor/bin/drush',
  ),
);
