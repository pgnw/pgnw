#!/bin/bash -ex

WORKSPACE=/Users/dsasser/Projects/pgnw
DOCROOT=/Users/dsasser/Projects/pgnw-pantheon/pgnw

# Package to push to docroot.
SITE_PACKAGE_PATH=$WORKSPACE/build/packages/package

# Rsync section.
RSYNC_CMD=`which rsync`
RSYNC_ARGS="-a --exclude=.git --exclude=web/sites/default/files --exclude=.gitignore --exclude=web/.htaccess --exclude=web/web.config --exclude=web/.eslintrc.json --exclude=web/.editorconfig --exclude=web/.eslintignore --exclude=web/sites/default/default.settings.php --delete-after"

cd $WORKSPACE
## Create build
fin exec npm install
#
## Build environment.
fin exec grunt --no-validate
#
## Package the build.
fin exec grunt package

# Perform the deployment.
${RSYNC_CMD} $RSYNC_ARGS $SITE_PACKAGE_PATH/ $DOCROOT || {
	echo 'Rsyncing build code to $DOCROOT.'
}
