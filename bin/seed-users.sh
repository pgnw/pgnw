#!/usr/bin/env bash
##
# Seed Users
#
# Creates dummy users for testing.
#
# Run from root of the code repository.
#
# This script is not automatically triggered by Grunt, and must be run/automated
# separately if desired in a given environment.
##

drush @pgnw user-create "pgnwadmin" --password="admin1" --mail="pgnwadmin@example.com"
drush @pgnw user-add-role "administrator" "pgnwadmin"
